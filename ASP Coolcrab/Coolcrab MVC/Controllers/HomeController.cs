﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Coolcrab.Entities;
using Microsoft.AspNet.Identity;
using System.Net;
using Coolcrab_MVC.Languages;

namespace Coolcrab_MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MyDay()
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return RedirectToAction("Index");

            return View();
        }

        public ActionResult MyTasks()
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return RedirectToAction("Index");

            CoolcrabEntities db = new CoolcrabEntities();

            ContentModel model = new ContentModel();
            model.content = new GetLanguageContent();


            model.tasks = (from p in db.Tasks where p.UserId == userId select p).ToList();

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}