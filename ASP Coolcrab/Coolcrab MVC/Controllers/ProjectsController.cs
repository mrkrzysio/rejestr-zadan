﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Coolcrab.Entities;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Runtime.CompilerServices;
using System.Web.DynamicData;

namespace Coolcrab_MVC.Controllers
{
    public class ProjectsController : Controller
    {
        [HttpGet]
        public ActionResult CreateProject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProject(Projects newProject)
        {
            string userId = User.Identity.GetUserId();

            newProject.OwnerId = userId;

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();
            db.Projects.Add(newProject);
            db.SaveChanges();

            return View();
        }

        [HttpGet]
        public ActionResult EditProject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EditProject(Projects updatedProject)
        {
            string userId = User.Identity.GetUserId();

            updatedProject.OwnerId = userId;

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();

            var project = (from p in db.Projects
                           where p.OwnerId == updatedProject.OwnerId && p.Id == updatedProject.Id
                           select p).SingleOrDefault();

            if (project != null)
            {
                db.Entry(project).CurrentValues.SetValues(updatedProject);
                db.SaveChanges();
            }

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public ActionResult DeleteProject(int projectId)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();
            Projects project = new Projects() { OwnerId = userId, Id = projectId };
            db.Projects.Attach(project);
            db.Projects.Remove(project);
            db.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public ActionResult ShowSingleProject(int projectId)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();
            if ((from p in db.ProjectMembers where p.ProjectId == projectId && p.UserId == userId select p).FirstOrDefault() == null)
            {
                var project = (from q in db.Projects where q.Id == projectId select q).FirstOrDefault();
                return View(project);
            }

            return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
        }

        public ActionResult GetUserProjects()
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();
            var projectsList = (from memb in db.ProjectMembers
                               join proj in db.Projects on memb.UserId equals userId
                                where memb.ProjectId == proj.Id
                                select proj).ToList();


            return View(projectsList);
        }
    }
}