﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Coolcrab.Entities;
using Microsoft.AspNet.Identity;
using System.Net;
using Coolcrab_MVC.Languages;
using System.Diagnostics;

namespace Coolcrab_MVC.Controllers
{
    public class ContentModel
    {
        public List<Tasks> tasks { get; set; }
        public List<TasksDates> dates { get; set; }
        public GetLanguageContent content { get; set; }
    }

    public class FinishedContentModel
    {
        public List<Tasks> tasks { get; set; }
        public Tasks task { get; set; }
        public List<FinishedTasks> finishedTasks { get; set; }
        public GetLanguageContent content { get; set; }
    }

    public class TasksController : Controller
    {
        [HttpGet]
        public ActionResult GetTasks(string finished)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();

            ContentModel model = new ContentModel();
            model.content = new GetLanguageContent();

            model.tasks = (from p in db.Tasks where p.UserId == userId select p).ToList();
            var tasksDates = (from r in db.Tasks
                              from u in db.TasksDates
                              where u.TaskId == r.Id
                              select u).ToList();

            var commonIds = (from p in model.tasks where p.Type == "common" select p.Id).ToList();
            var dailyIds = (from p in model.tasks where p.Type == "daily" select p.Id).ToList();
            var weeklyIds = (from p in model.tasks where p.Type == "weekly" select p.Id).ToList();

            var commonDates = tasksDates.Where(p => commonIds.Contains(p.TaskId)).OrderBy(p => p.TaskId).ToList();
            var dailyDates = tasksDates.Where(p => dailyIds.Contains(p.TaskId)).OrderBy(p => p.TaskId).ToList();
            var weeklyDates = tasksDates.Where(p => weeklyIds.Contains(p.TaskId)).OrderBy(p => p.TaskId).ToList();

            model.dates = new List<TasksDates>();
            model.dates.AddRange(commonDates);

            DateTime weekBeginning = DateTime.Now;
            weekBeginning = weekBeginning.AddSeconds(-(((int)DateTime.Now.DayOfWeek - 1) * 86400 + weekBeginning.TimeOfDay.TotalSeconds));

            DateTime dayBeginning = DateTime.Now;
            dayBeginning = dayBeginning.AddSeconds(-dayBeginning.TimeOfDay.TotalSeconds);

            TimeSpan currentTimeOfDay = DateTime.Now.TimeOfDay;
            DateTime currentDate = DateTime.Now;
            DateTime timeBeginning = new DateTime(1970, 1, 1, 0, 0, 0).Date;

            int lastTaskId = -1;

            foreach (TasksDates date in dailyDates)
            {
                if (date.TaskId == lastTaskId)
                    continue;

                lastTaskId = date.TaskId;

                var cyclicTaskDates = dailyDates.Where(p => p.TaskId == date.TaskId).OrderBy(p => p.Deadline).ToList(); // Wybieram daty należące do danego zadania

                for (int i = 0; i < cyclicTaskDates.Count; i++)
                {
                    if (cyclicTaskDates[i].Deadline.TimeOfDay > currentTimeOfDay) // Wybieram najbliższą nadchodzącą date cykliczną
                    {
                        var closestDate = dayBeginning.Add(cyclicTaskDates[i].Deadline - timeBeginning).AddSeconds(2);
                        model.dates.Add(new TasksDates() { TaskId = date.TaskId, Deadline = closestDate });
                        break;
                    }
                    else if (i == cyclicTaskDates.Count - 1)
                    {
                        var closestDate = dayBeginning.Add(cyclicTaskDates[0].Deadline - timeBeginning).AddDays(1).AddSeconds(2);
                        model.dates.Add(new TasksDates() { TaskId = date.TaskId, Deadline = closestDate });
                        break;
                    }
                }
            }

            foreach (TasksDates date in weeklyDates)
            {
                if (date.TaskId == lastTaskId)
                    continue;

                lastTaskId = date.TaskId;

                var cyclicTaskDates = weeklyDates.Where(p => p.TaskId == date.TaskId).OrderBy(p => p.Deadline).ToList(); // Wybieram daty należące do danego zadania

                for (int i = 0; i < cyclicTaskDates.Count; i++)
                {
                    var closestDate = weekBeginning.Add(cyclicTaskDates[i].Deadline - timeBeginning).AddSeconds(2);
                    if (closestDate > currentDate) // Wybieram najbliższą nadchodzącą date cykliczną
                    {
                        model.dates.Add(new TasksDates() { TaskId = date.TaskId, Deadline = closestDate });
                        break;
                    }
                    else if (i == cyclicTaskDates.Count - 1)
                    {
                        closestDate = weekBeginning.Add(cyclicTaskDates[0].Deadline - timeBeginning).AddDays(7).AddSeconds(2);
                        model.dates.Add(new TasksDates() { TaskId = date.TaskId, Deadline = closestDate });
                        break;
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult GetFinishedTasks()
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();

            FinishedContentModel model = new FinishedContentModel();
            model.content = new GetLanguageContent();

            model.tasks = (from r in db.Tasks
                           from u in db.FinishedTasks
                           where r.Id == u.TaskId
                           select r).ToList();

            model.finishedTasks = (from r in db.Tasks
                                 from u in db.FinishedTasks
                                 where u.TaskId == r.Id
                                 select u).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTask(Tasks newTask, List<DateTime> dates)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            newTask.UserId = userId;

            CoolcrabEntities db = new CoolcrabEntities();
            db.Tasks.Add(newTask);
            TasksDates taskDate = new TasksDates();
            taskDate.TaskId = newTask.Id;
            foreach (var date in dates)
            {
                taskDate.Deadline = date;
                db.TasksDates.Add(taskDate);
                db.SaveChanges();
            }

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MoveTask(FinishedTasks finishedTask, string Destination)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();

            var result = (from p in db.Tasks where p.UserId == userId && p.Id == finishedTask.TaskId select p);

            if (!result.Any())
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            if (Destination == "finished-tasks")
            {
                finishedTask.CompletionDate = DateTime.Now;
                db.FinishedTasks.Add(finishedTask);
            }
            else
            {
                var itemToRemove = db.FinishedTasks.SingleOrDefault(x => x.TaskId == finishedTask.TaskId && x.Deadline == finishedTask.Deadline);

                if (itemToRemove != null)
                {
                    db.FinishedTasks.Remove(itemToRemove);
                }
            }
            db.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        [HttpPost]
        public ActionResult EditTask(Tasks updatedTask)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();

            var task = db.Tasks.Find(updatedTask.UserId, updatedTask.Id);

            if (task != null)
            {
                db.Entry(task).CurrentValues.SetValues(updatedTask);
                db.SaveChanges();
            }

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public ActionResult DeleteTask(int taskId)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();
            Tasks task = new Tasks() { UserId = userId, Id = taskId };
            db.Tasks.Attach(task);
            db.Tasks.Remove(task);
            db.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public ActionResult ShowSingleTask(int taskId)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();
            var task = (from p in db.Tasks where p.UserId == userId && p.Id == taskId select p).ToList();

            return View(task);
        }

        public ActionResult GetUserTasks(int? projectId = null)
        {
            string userId = User.Identity.GetUserId();

            if (userId == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            CoolcrabEntities db = new CoolcrabEntities();
            var tasksList = (from p in db.Tasks where p.UserId == userId && p.ProjectId == projectId select p).ToList();

            return View(tasksList);
        }
    }
}