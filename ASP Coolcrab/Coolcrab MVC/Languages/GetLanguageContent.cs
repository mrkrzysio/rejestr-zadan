﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coolcrab_MVC.Languages
{
    public class GetLanguageContent
    {
        private Content c;

        public GetLanguageContent()
        {
            c = new Polish();
        }

        public string Priority(int p)
        {
            if (p == 1)
                return c.priority_1;
            else if (p == 3)
                return c.priority_3;
            else
                return c.priority_2;
        }

        public string Hardlevel(int p)
        {
            if (p == 1)
                return c.hardlevel_1;
            else if (p == 3)
                return c.hardlevel_3;
            else
                return c.hardlevel_2;
        }

        public string Type(string p)
        {
            if (p == "daily")
                return c.typeDaily;
            else if (p == "weekly")
                return c.typeWeekly;
            else
                return c.typeCommon;
        }
    }
}