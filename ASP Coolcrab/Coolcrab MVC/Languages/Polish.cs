﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coolcrab_MVC.Languages
{
    class Polish : Content
    {
        public Polish()
        {
            priority_1 = "Niski";
            priority_2 = "Średni";
            priority_3 = "Wysoki";

            hardlevel_1 = "Łatwy";
            hardlevel_2 = "Normalny";
            hardlevel_3 = "Trudny";

            typeCommon = "Zwykłe";
            typeDaily = "Dzienne";
            typeWeekly = "Tygodniowe";
        }
    }
}