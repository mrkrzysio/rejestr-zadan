﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Coolcrab_MVC.Startup))]
namespace Coolcrab_MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
