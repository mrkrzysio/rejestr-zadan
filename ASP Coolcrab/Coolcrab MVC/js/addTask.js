$("#radioCommon").click(function(){
    $("#pickCyclicDate").hide();

    $("#pickDate").show();

    $("#cyclicDates").empty();
});

$("#radioDaily").click(function(){
    $("#pickDate").hide();

    $("#pickCyclicDate").show(); 

    $("#weeklyPicker").hide();
    $("#dailyPicker").show();

    $("#cyclicDates").empty();
});

$("#radioWeekly").click(function(){
    $("#pickDate").hide();

    $("#pickCyclicDate").show(); 

    $("#dailyPicker").hide();
    $("#weeklyPicker").show();

    $("#cyclicDates").empty();
});

$("#addCyclicDate").click(function () {
    try {
        if ($("#radioDaily input").is(':checked')) {
            let cyclicDate = $("#dailyPicker input").val();
            let timeOffset = cyclicDate.split(":");
            timeOffset = timeOffset[0] * 60 + timeOffset[1] * 1;
            $("#cyclicDates").prepend("<option value='" + timeOffset + "'>" + cyclicDate + "</option>");
        }
        else if ($("#radioWeekly input").is(':checked')) {
            let cyclicDate = $("#weeklyTimePicker").val();
            let timeOffset = cyclicDate.split(":");
            timeOffset = timeOffset[0] * 60 + timeOffset[1] * 1;
            timeOffset += $("#dayOfWeekPicker").prop("value") * 1440;
            cyclicDate = $("#dayOfWeekPicker option:selected").text() + " " + cyclicDate;
            $("#cyclicDates").prepend("<option value='" + timeOffset + "'>" + cyclicDate + "</option>");
        }
    }
    catch (err) { console.log("error");  return; };
});

$("#removeCyclicDate").click(function () {
    $("#cyclicDates option:selected").remove();
});

$("#clearCyclicDates").click(function () {
    $("#cyclicDates").empty();
});

function getDateTimeNow() {
    let now = new Date();
    let dd = now.getDate();
    let mm = now.getMonth() + 1; //January is 0!
    let yyyy = now.getFullYear();

    if(dd<10) {
        dd = '0'+dd;
    } 

    if(mm<10) {
        mm = '0'+mm;
    }     
    
    now = dd + '.' + mm + '.' + yyyy + "23:59";
    
    $("#datePicker input").val(now);
}

function createNewTask(projectId) {
    projectId = typeof projectId !== 'undefined' ? a : -1;
    let shortDesc = $("#shortDescription").val();
    let desc = $("#description").val();

    let priority = $('input[name=priority]:checked', '#addTaskModal').val();
    let hardlevel = $('input[name=hardlevel]:checked', '#addTaskModal').val();
    let type = $('input[name=type]:checked', '#addTaskModal').val();

    let token = $('#addTaskModal input[name=__RequestVerificationToken]').val();

    let dates = [];

    if (type !== "common") {
        let cyclicDates = $("#cyclicDates option");

        cyclicDates.each(function (index) {
            let newDate = new Date(new Date(0).getTime() + $(this).val() * 60000);
            newDate = new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
            dates.push(newDate.getDate() + ".01.1970 " + newDate.getHours() + ":" + newDate.getMinutes());
        });
    }
    else {
        dates.push($("#datePicker input").val());
    }

    let request = $.ajax({
        url: "/Tasks/CreateTask/",
        method: "POST",
        data: { __RequestVerificationToken: token, ProjectId: projectId, ShortDescription: shortDesc, Description: desc, Priority: priority, Hardlevel: hardlevel, Type: type, Dates: dates },
    });

    request.done(function (msg) {
        $("#log").html(msg);
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}
