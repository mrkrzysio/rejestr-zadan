﻿$( document ).ready(function() {
    $("#taskCount").text("Wszystkie zadania [" + $("#tasks .task:visible").length + "]");
});

function resetFilterMenu() {
    $("#filterMenu input").prop('checked', false);
    $("#tasks .task").show();
    $("#taskCount").text("Wszystkie zadania [" + $("#tasks .task:visible").length + "]");
}

function filterTasks() {
    let tasks = $("#tasks .task");
    tasks.show();

    if ($("#filterMenu input:checkbox:checked").length === 0) {
        $("#taskCount").text("Wszystkie zadania [" + $("#tasks .task:visible").length + "]");
        return;
    }
    
    if ($("#filterMenu #priority input:checkbox:checked").length > 0) {

        if (!$("#filterMenu input[value=lowPriority]").prop('checked'))
            $("#tasks .task[priority=1]").hide();

        if (!$("#filterMenu input[value=normalPriority]").prop('checked'))
            $("#tasks .task[priority=2]").hide();

        if (!$("#filterMenu input[value=highPriority]").prop('checked'))
            $("#tasks .task[priority=3]").hide();
    }

    if ($("#filterMenu #hardlevel input:checkbox:checked").length > 0) {

        if (!$("#filterMenu input[value=easyLevel]").prop('checked'))
            $("#tasks .task[hardlevel=1]").hide();

        if (!$("#filterMenu input[value=normalLevel]").prop('checked'))
            $("#tasks .task[hardlevel=2]").hide();

        if (!$("#filterMenu input[value=hardLevel]").prop('checked'))
            $("#tasks .task[hardlevel=3]").hide();
    }

    if ($("#filterMenu #type input:checkbox:checked").length > 0) {
        if (!$("#filterMenu input[value=common]").prop('checked'))
            $("#tasks .task[taskType=common]").hide();
           
        if (!$("#filterMenu input[value=daily]").prop('checked'))
            $("#tasks .task[taskType=daily]").hide();

        if (!$("#filterMenu input[value=weekly]").prop('checked'))
            $("#tasks .task[taskType=weekly]").hide();
    }

    
    if ($("#filterMenu #deadline input:checkbox:checked").length > 0) {
        if ($("#filterMenu input[value=nextWeek]").prop('checked')) {

            let nextWeek = new Date();
            nextWeek.setDate(nextWeek.getDate() + 7);

            tasks.each(function (index) {
                if (new Date($(this).attr("datetime")) > nextWeek)
                    $(this).hide();
            });
        }

        else if ($("#filterMenu input[value=nextDay]").prop('checked')) {

            let nextDayDate = new Date();
            nextDayDate.setDate(nextDayDate.getDate() + 1);

            tasks.each(function (index) {
               if (new Date($(this).attr("datetime")) > nextDayDate)
                    $(this).hide();
            });
        }
    }

    $("#taskCount").text("Zadania [" + $("#tasks .task:visible").length + "]");
}

$("#filterMenu button").click(resetFilterMenu);

$("#filterMenu input").click(filterTasks);


//************* Ajax ********************

function loadTasks() {
    let request = $.ajax({
        url: "/Tasks/GetTasks",
        method: "GET",
    });

    request.done(function (data) {
        $("#tasks").html(data);
        $('[data-toggle="tooltip"]').tooltip();
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

    request = $.ajax({
        url: "/Tasks/GetFinishedTasks",
        method: "GET",
    });

    request.done(function (data) {
        $("#finished-tasks").html(data);
        $('[data-toggle="tooltip"]').tooltip();
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

loadTasks();

function moveTask(task, destination) {
    let token = $('input[name=__RequestVerificationToken]').val();
    let deadline = $(task).attr("datetime");

    let request = $.ajax({
        url: "/Tasks/MoveTask/",
        method: "POST",
        data: { __RequestVerificationToken: token, TaskId: task.id.replace("task_", ""), Deadline: deadline, Destination: destination},
    });

    request.done(function (msg) {
        $("#log").html(msg);
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

//************* Drag & Drop ********************

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    var data = ev.dataTransfer.getData("text");

    movedEl = document.getElementById(data);
    var dropBox = ev.target;
    while (dropBox.id !== 'finished-tasks' && dropBox.id !== 'tasks') {
        dropBox = dropBox.parentNode;
    }

    if (dropBox.id === movedEl.parentElement.id)
        return;

    dropBox.prepend(movedEl);
    ev.preventDefault();
    $(movedEl).hide();
    $(movedEl).show("slow");

    if ($("#filterMenu input:checkbox:checked").length === 0)
        $("#taskCount").text("Wszystkie zadania [" + $("#tasks .task:visible").length + "]");
    else
        $("#taskCount").text("Zadania [" + $("#tasks .task:visible").length + "]");

    moveTask(movedEl, dropBox.id);
}

$('#finished-tasks').click(function (e) {
    e.stopPropagation();
});