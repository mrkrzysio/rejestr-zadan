//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Coolcrab.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tasks
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tasks()
        {
            this.Priority = 2;
            this.HardLevel = 2;
            this.Type = "common";
            this.CyclicDates = new HashSet<TasksDates>();
            this.FinishedTasks = new HashSet<FinishedTasks>();
        }
    
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string UserId { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public byte Priority { get; set; }
        public byte HardLevel { get; set; }
        public string Type { get; set; }
    
        public virtual AspNetUsers AspNetUsers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TasksDates> CyclicDates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FinishedTasks> FinishedTasks { get; set; }
    }
}
