﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace AndroidClient
{
    [Activity(Label = "AndroidClient", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            System.Console.WriteLine("oncreate");
            SetContentView (Resource.Layout.Main);
        }
    }
}

