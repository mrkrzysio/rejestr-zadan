﻿namespace Coolcrab.Local
{
    public enum DataValidationResult
    {
        Success,
        Invalid,
        Empty,
        TooLong,
    }
}