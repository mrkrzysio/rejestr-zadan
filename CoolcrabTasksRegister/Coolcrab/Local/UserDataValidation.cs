﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Coolcrab.Local
{
    public class UserDataValidation
    {
        public static readonly int LoginMaxLength = 128;
        public static readonly int PasswordMaxLength = 64;
        public static readonly int EmailMaxLength = 64;
             
        private readonly string LoginTestPattern = @"^[a-zA-Z0-9_]+$";  // dopasuje jedynie ciągi znaków zawierające litery, liczby oraz znak '_'
        private readonly string PasswordTestPattern = @"\s";            // dopasuje wszystkie wysąpienia białych znaków, jeżeli wystąpień nie będzie
                                                                        //  oznacza to że hasło jest poprawne
        private Regex LoginTestRegex;
        private Regex PasswordTestRegex;

        public UserDataValidation()
        {
            LoginTestRegex = new Regex(LoginTestPattern);
            PasswordTestRegex = new Regex(PasswordTestPattern);
        }

        public DataValidationResult TestLogin(string login)
        {
            if (IsEmpty(login))
            {
                return DataValidationResult.Empty;
            }
            else if (login.Length > LoginMaxLength)
            {
                return DataValidationResult.TooLong;
            }
            else if (IsLoginValid(login))
            {
                return DataValidationResult.Success;
            }
            else
            {
                return DataValidationResult.Invalid;
            }
        }

        private bool IsLoginValid(string login)
        {
            return LoginTestRegex.IsMatch(login);
        }

        public DataValidationResult TestPassword(string password)
        {
            if (IsEmpty(password))
            {
                return DataValidationResult.Empty;
            }
            else if (password.Length > PasswordMaxLength)
            {
                return DataValidationResult.TooLong;
            }
            else if (IsPasswordValid(password))
            {
                return DataValidationResult.Success;
            }
            else
            {
                return DataValidationResult.Invalid;
            }
        }

        private bool IsPasswordValid(string password)
        {
            return !PasswordTestRegex.IsMatch(password);
        }

        public DataValidationResult TestEmail(string email)
        {
            if(IsEmpty(email))
            {
                return DataValidationResult.Empty;
            }
            else if (email.Length > EmailMaxLength)
            {
                return DataValidationResult.TooLong;
            }
            else if (IsEmailValid(email))
            {
                return DataValidationResult.Success;
            }
            else
            {
                return DataValidationResult.Invalid;
            }
        }

        private bool IsEmailValid(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private bool IsEmpty(string str)
        {
            return !str.Any();
        }
    }
}
