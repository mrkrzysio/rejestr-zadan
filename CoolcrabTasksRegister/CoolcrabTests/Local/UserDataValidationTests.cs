﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Coolcrab.Local;

namespace CoolcrabTests.Local
{
    [TestClass]
    public class UserDataValidationTests
    {
        private UserDataValidation UserDataValidation;

        public UserDataValidationTests()
        {
            UserDataValidation = new UserDataValidation();
        }

        [TestMethod]
        public void TestLogin_ValidLogin_Success()
        {
            string validLogin = "login123_ABC";
            Assert.AreEqual(DataValidationResult.Success, UserDataValidation.TestLogin(validLogin));
        }

        [TestMethod]
        public void TestLogin_InvalidLogin_Invalid()
        {
            string invalidLogin = "login!$%^";
            Assert.AreEqual(DataValidationResult.Invalid, UserDataValidation.TestLogin(invalidLogin));
        }

        [TestMethod]
        public void TestLogin_EmptyLogin_Empty()
        {
            string emptyLogin = string.Empty;
            Assert.AreEqual(DataValidationResult.Empty, UserDataValidation.TestLogin(emptyLogin));
        }

        [TestMethod]
        public void TestLogin_TooLongLogin_TooLong()
        {
            string tooLongLogin = "123456789_0123456789_0123456789_0123456789_0123456789_" +
                                  "123456789_0123456789_0123456789_0123456789_0123456789_" +
                                  "123456789_0123456789_0123456789_0123456789_0123456789_";
            Assert.AreEqual(DataValidationResult.TooLong, UserDataValidation.TestLogin(tooLongLogin));
        }

        [TestMethod]
        public void TestPassword_ValidPassword_Success()
        {
            string validPassword = "zaq1@WSX!!__";
            Assert.AreEqual(DataValidationResult.Success, UserDataValidation.TestPassword(validPassword));
        }

        [TestMethod]
        public void TestPassword_InvalidPassword_Invalid()
        {
            string whitespacesPassword = "zaq1  @WSX!__";
            Assert.AreEqual(DataValidationResult.Invalid, UserDataValidation.TestPassword(whitespacesPassword));
        }

        [TestMethod]
        public void TestPassword_EmptyPassword_Empty()
        {
            string emptyPassword = string.Empty;
            Assert.AreEqual(DataValidationResult.Empty, UserDataValidation.TestPassword(emptyPassword));
        }

        [TestMethod]
        public void TestPassword_TooLongPassword_TooLong()
        {
            string tooLongPassword = "123456789_0123456789_0123456789_0123456789_0123456789_" +
                                  "123456789_0123456789_0123456789_0123456789_0123456789_";
            Assert.AreEqual(DataValidationResult.TooLong, UserDataValidation.TestPassword(tooLongPassword));
        }

        [TestMethod]
        public void TestEmail_ValidEmail_Success()
        {
            string validEmail = "jankowalski@xyz.com";
            Assert.AreEqual(DataValidationResult.Success, UserDataValidation.TestEmail(validEmail));
        }

        [TestMethod]
        public void TestEmail_InvalidEmail_Invalid()
        {
            string invalidEmail = "jankowalski&xyz.com";
            Assert.AreEqual(DataValidationResult.Invalid, UserDataValidation.TestEmail(invalidEmail));
        }

        [TestMethod]
        public void TestEmail_EmptyEmail_Empty()
        {
            string emptyEmail = string.Empty;
            Assert.AreEqual(DataValidationResult.Empty, UserDataValidation.TestEmail(emptyEmail));
        }

        [TestMethod]
        public void TestEmail_TooLongEmail_TooLong()
        {
            string tooLongEmail = "123456789_0123456789_0123456789_0123456789_0123456789_" +
                                  "123456789_0123456789_0123456789_0123456789_0123456789_";
            Assert.AreEqual(DataValidationResult.TooLong, UserDataValidation.TestEmail(tooLongEmail));
        }
    }
}
